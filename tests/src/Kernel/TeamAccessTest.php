<?php

namespace Drupal\Tests\violinist_teams\Kernel;

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\node\Entity\Node;
use Drupal\user\Entity\User;
use Symfony\Component\Routing\Route;

/**
 * Tests for the team access service.
 *
 * @group violinist_teams
 */
class TeamAccessTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();
    // Just make sure we have a completely different user with user id 1, since
    // otherwise these permission checks will always allow access to the first
    // user of the test.
    $user = User::create([
      'name' => 'admin',
      'mail' => 'admin@example.com',
    ]);
    $user->save();
  }

  /**
   * Test access.
   *
   * @dataProvider accessProvider
   */
  public function testAdminAccessUserMember($user_index, $type, $expected_class) {
    /** @var \Drupal\violinist_teams\TeamNode $node */
    $node = Node::create([
      'type' => $this->nodeType->id(),
      'title' => 'test',
    ]);
    $node->save();
    /** @var \Drupal\violinist_teams\Access\TeamAccess $access_service */
    $access_service = $this->container->get('violinist_teams.access_check');
    $users = [];
    $user = User::create([
      'name' => 'test',
      'mail' => 'test@test.com',
    ]);
    $user->save();
    $node->appendMember($user)->save();
    $users[] = $user;
    $user = User::create([
      'name' => 'test2',
      'mail' => 'test2@test.com',
    ]);
    $user->save();
    $node->appendAdmin($user)->save();
    $users[] = $user;
    $use_user = $users[$user_index];
    $this->container->get('current_user')->setAccount($use_user);
    $route = $this->createMock(Route::class);
    $route->method('getRequirement')->willReturn($type);
    $route_match = $this->createMock(RouteMatchInterface::class);
    $route_match->method('getParameter')->willReturn($node);
    $access = $access_service->access($route, $route_match);
    self::assertInstanceOf($expected_class, $access);
  }

  /**
   * Data provider for access tests.
   */
  public function accessProvider() {
    return [
      [0, 'admin', 'Drupal\Core\Access\AccessResultNeutral'],
      [0, 'member', 'Drupal\Core\Access\AccessResultAllowed'],
      [1, 'admin', 'Drupal\Core\Access\AccessResultAllowed'],
      [1, 'member', 'Drupal\Core\Access\AccessResultAllowed'],
    ];
  }

}
