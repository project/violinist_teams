<?php

namespace Drupal\Tests\violinist_teams\Kernel;

use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\violinist_teams\TeamNode;

/**
 * A trait for setting up team fields.
 */
trait TeamFieldKernelSetupTrait {

  /**
   * Create fields we need on the team content type.
   */
  public function createTeamFields() {
    // The field we use for notifications.
    $fieldStorage = FieldStorageConfig::create([
      'field_name' => 'field_team_notification_emails',
      'entity_type' => 'node',
      'type' => 'string_long',
      'cardinality' => 1,
    ]);
    $fieldStorage->save();
    $field = FieldConfig::create([
      'field_storage' => $fieldStorage,
      'bundle' => TeamNode::NODE_TYPE,
      'field_type' => 'string_long',
      'required' => FALSE,
    ]);
    $field->save();

    // Create the field for holding members.
    $fieldStorage = FieldStorageConfig::create([
      'field_name' => 'field_team_members',
      'entity_type' => 'node',
      'type' => 'entity_reference',
      'cardinality' => -1,
      'settings' => [
        'target_type' => 'user',
      ],
    ]);
    $fieldStorage->save();
    $field = FieldConfig::create([
      'field_storage' => $fieldStorage,
      'bundle' => TeamNode::NODE_TYPE,
      'field_type' => 'entity_reference',
      'required' => FALSE,
      'settings' => [
        ['handler' => 'default:user'],
      ],
    ]);
    $field->save();

    // Create the field for holding admins.
    $fieldStorage = FieldStorageConfig::create([
      'field_name' => 'field_team_admins',
      'entity_type' => 'node',
      'type' => 'entity_reference',
      'cardinality' => -1,
      'settings' => [
        'target_type' => 'user',
      ],
    ]);
    $fieldStorage->save();
    $field = FieldConfig::create([
      'field_storage' => $fieldStorage,
      'bundle' => TeamNode::NODE_TYPE,
      'field_type' => 'entity_reference',
      'required' => FALSE,
      'settings' => [
        ['handler' => 'default:user'],
      ],
    ]);
    $field->save();

    // Slack notifications.
    $fieldStorage = FieldStorageConfig::create([
      'field_name' => TeamNode::NOTIFICATION_ENABLED_FIELD,
      'entity_type' => 'node',
      'type' => 'boolean',
      'cardinality' => 1,
    ]);
    $fieldStorage->save();
    $field = FieldConfig::create([
      'field_storage' => $fieldStorage,
      'bundle' => TeamNode::NODE_TYPE,
      'field_type' => 'boolean',
      'required' => FALSE,
      'default_value' => [
        [
          'value' => 0,
        ],
      ],
    ]);
    $field->save();
  }

}
