<?php

namespace Drupal\Tests\violinist_teams\Kernel;

use Drupal\KernelTests\KernelTestBase as CoreKernelTestBase;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\node\Entity\NodeType;
use Drupal\violinist_teams\TeamNode;

/**
 * Base class for our tests.
 */
abstract class KernelTestBase extends CoreKernelTestBase {

  use TeamFieldKernelSetupTrait;

  /**
   * Node type.
   *
   * @var \Drupal\node\Entity\NodeType
   */
  protected $nodeType;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'violinist_teams',
    'node',
    'system',
    'user',
    'field',
    'text',
  ];

  /**
   * {@inheritdoc}
   */
  public function setUp() : void {
    parent::setUp();
    $this->installSchema('node', ['node_access']);
    $node_type = NodeType::create([
      'type' => TeamNode::NODE_TYPE,
    ]);
    $node_type->save();
    $this->nodeType = $node_type;
    $this->installEntitySchema('user');
    $this->installEntitySchema('node');
    $this->installConfig(static::$modules);

    $this->createTeamFields();

    // The field we use for max concurrent notifications.
    $fieldStorage = FieldStorageConfig::create([
      'field_name' => 'field_max_concurrent_emails',
      'entity_type' => 'node',
      'type' => 'string_long',
      'cardinality' => 1,
    ]);
    $fieldStorage->save();
    $field = FieldConfig::create([
      'field_storage' => $fieldStorage,
      'bundle' => $this->nodeType->id(),
      'field_type' => 'string_long',
      'required' => FALSE,
    ]);
    $field->save();

    // Licences.
    $fieldStorage = FieldStorageConfig::create([
      'field_name' => 'field_licences',
      'entity_type' => 'node',
      'type' => 'string_long',
      'cardinality' => -1,
    ]);
    $fieldStorage->save();
    $field = FieldConfig::create([
      'field_storage' => $fieldStorage,
      'bundle' => $this->nodeType->id(),
      'field_type' => 'string_long',
      'required' => FALSE,
    ]);
    $field->save();

    // The field we use for env vars.
    $fieldStorage = FieldStorageConfig::create([
      'field_name' => 'field_environment_variables',
      'entity_type' => 'node',
      'type' => 'string_long',
      'cardinality' => 1,
    ]);
    $fieldStorage->save();
    $field = FieldConfig::create([
      'field_storage' => $fieldStorage,
      'bundle' => $this->nodeType->id(),
      'field_type' => 'string_long',
      'required' => FALSE,
    ]);
    $field->save();

    // The field we use for PR templates.
    $fieldStorage = FieldStorageConfig::create([
      'field_name' => 'field_pull_request_template',
      'entity_type' => 'node',
      'type' => 'string_long',
      'cardinality' => 1,
    ]);
    $fieldStorage->save();
    $field = FieldConfig::create([
      'field_storage' => $fieldStorage,
      'bundle' => $this->nodeType->id(),
      'field_type' => 'string_long',
      'required' => FALSE,
    ]);
    $field->save();

    // Slack notifications.
    $fieldStorage = FieldStorageConfig::create([
      'field_name' => 'field_slack_notifications',
      'entity_type' => 'node',
      'type' => 'boolean',
      'cardinality' => 1,
    ]);
    $fieldStorage->save();
    $field = FieldConfig::create([
      'field_storage' => $fieldStorage,
      'bundle' => $this->nodeType->id(),
      'field_type' => 'boolean',
      'required' => FALSE,
      'default_value' => [
        [
          'value' => 0,
        ],
      ],
    ]);
    $field->save();

    // Billing enabled.
    $fieldStorage = FieldStorageConfig::create([
      'field_name' => 'field_send_receipts',
      'entity_type' => 'node',
      'type' => 'boolean',
      'cardinality' => 1,
    ]);
    $fieldStorage->save();
    $field = FieldConfig::create([
      'field_storage' => $fieldStorage,
      'bundle' => $this->nodeType->id(),
      'field_type' => 'boolean',
      'required' => FALSE,
      'default_value' => [
        [
          'value' => 0,
        ],
      ],
    ]);
    $field->save();

    // Project limits.
    $fieldStorage = FieldStorageConfig::create([
      'field_name' => 'field_project_limit',
      'entity_type' => 'node',
      'type' => 'integer',
      'cardinality' => 1,
    ]);
    $fieldStorage->save();
    $field = FieldConfig::create([
      'field_storage' => $fieldStorage,
      'bundle' => $this->nodeType->id(),
      'field_type' => 'integer',
      'required' => FALSE,
    ]);
    $field->save();

    // Projects override.
    $fieldStorage = FieldStorageConfig::create([
      'field_name' => 'field_team_allowed_override',
      'entity_type' => 'node',
      'type' => 'integer',
      'cardinality' => 1,
    ]);
    $fieldStorage->save();
    $field = FieldConfig::create([
      'field_storage' => $fieldStorage,
      'bundle' => $this->nodeType->id(),
      'field_type' => 'integer',
      'required' => FALSE,
    ]);
    $field->save();

    // Plan field.
    $fieldStorage = FieldStorageConfig::create([
      'field_name' => 'field_plan',
      'entity_type' => 'node',
      'type' => 'string',
      'cardinality' => 1,
    ]);
    $fieldStorage->save();
    $field = FieldConfig::create([
      'field_storage' => $fieldStorage,
      'bundle' => $this->nodeType->id(),
      'field_type' => 'string',
      'required' => FALSE,
    ]);
    $field->save();

    // Billing email.
    $fieldStorage = FieldStorageConfig::create([
      'field_name' => 'field_billing_email',
      'entity_type' => 'node',
      'type' => 'email',
      'cardinality' => 1,
    ]);
    $fieldStorage->save();
    $field = FieldConfig::create([
      'field_storage' => $fieldStorage,
      'bundle' => $this->nodeType->id(),
      'field_type' => 'email',
      'required' => FALSE,
    ]);
    $field->save();

    // Slack webhook.
    $fieldStorage = FieldStorageConfig::create([
      'field_name' => 'field_slack_webhook',
      'entity_type' => 'node',
      'type' => 'string',
      'cardinality' => 1,
    ]);
    $fieldStorage->save();
    $field = FieldConfig::create([
      'field_storage' => $fieldStorage,
      'bundle' => $this->nodeType->id(),
      'field_type' => 'string',
      'required' => FALSE,
    ]);
    $field->save();
    $this->installSchema('user', ['users_data']);

    // The project content type.
    $node_type = NodeType::create([
      'type' => 'project',
    ]);
    $node_type->save();

    // And the field "field_team".
    $fieldStorage = FieldStorageConfig::create([
      'field_name' => 'field_team',
      'entity_type' => 'node',
      'type' => 'entity_reference',
      'cardinality' => -1,
      'settings' => [
        'target_type' => 'node',
      ],
    ]);
    $fieldStorage->save();
    $field = FieldConfig::create([
      'field_storage' => $fieldStorage,
      'bundle' => 'project',
      'field_type' => 'entity_reference',
      'required' => FALSE,
      'settings' => [
        ['handler' => 'default:node'],
      ],
    ]);
    $field->save();
  }

}
