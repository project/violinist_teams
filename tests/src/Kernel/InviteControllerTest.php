<?php

namespace Drupal\Tests\violinist_teams\Kernel;

use Drupal\node\Entity\Node;
use Drupal\user\Entity\User;
use Drupal\user\UserInterface;
use Drupal\violinist_teams\Controller\InviteController;
use Drupal\violinist_teams\TeamNode;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Tests for the invite controller.
 *
 * @group violinist_teams
 */
class InviteControllerTest extends KernelTestBase {

  /**
   * Invite controller.
   *
   * @var \Drupal\violinist_teams\Controller\InviteController
   */
  private InviteController $controller;

  /**
   * Team node.
   *
   * @var \Drupal\violinist_teams\TeamNode
   */
  private TeamNode $team;

  /**
   * User.
   *
   * @var \Drupal\user\UserInterface
   */
  private UserInterface $user;

  /**
   * {@inheritdoc}
   */
  public function setUp() : void {
    parent::setUp();
    $this->controller = InviteController::create($this->container);
    $this->team = Node::create([
      'type' => 'team',
      'title' => 'test',
    ]);
    $this->team->save();

    $user = User::create([
      'name' => 'test',
      'mail' => 'test@test.com',
    ]);
    $user->save();
    $this->user = $user;
  }

  /**
   * Test with a non-existing team.
   */
  public function testNonExistingTeam() {
    $controller = $this->controller;
    $this->expectException(AccessDeniedHttpException::class);
    $this->expectExceptionMessage('No team loaded from id 1234');
    $controller(1234, 'member', time(), 'hash');
  }

  /**
   * Test with an invalid hash.
   */
  public function testInvalidRandomHashCall() {
    $controller = $this->controller;
    $this->expectExceptionMessage('Invalid hash');
    $this->expectException(AccessDeniedHttpException::class);
    $controller($this->team->id(), 'member', time(), 'hash');
  }

  /**
   * Test with a valid hash and a logged in user.
   */
  public function testAssignLoggedinUser() {
    /** @var \Drupal\Core\Session\AccountProxy $current_user */
    $current_user = $this->container->get('current_user');
    $current_user->setAccount($this->user);
    $controller = $this->controller;
    $response = $controller($this->team->id(), 'member', time(), $this->container->get('violinist_teams.team_manager')->getInviteHash($this->team, 'member', time()));
    self::assertInstanceOf('Symfony\Component\HttpFoundation\RedirectResponse', $response);
    // Reload object since it was saved.
    /** @var \Drupal\violinist_teams\TeamNode $team */
    $team = $this->container->get('entity_type.manager')->getStorage('node')->load($this->team->id());
    self::assertEquals(TRUE, in_array($this->user->id(), $team->getMemberIds()));
  }

  /**
   * Test with a valid hash and not a logged in user.
   */
  public function testSetSessionNotLoggedIn() {
    /** @var \Symfony\Component\HttpFoundation\Session\SessionInterface $session */
    $session = $this->container->get('session');
    $session->remove(InviteController::INVITE_DATA);
    self::assertEmpty($session->get(InviteController::INVITE_DATA));
    $controller = $this->controller;
    $controller($this->team->id(), 'admin', time(), $this->container->get('violinist_teams.team_manager')->getInviteHash($this->team, 'admin', time()));
    $session_data = $session->get(InviteController::INVITE_DATA);
    self::assertEquals($this->team->id(), $session_data['team_id']);
    self::assertEquals('admin', $session_data['membership_type']);
  }

}
