<?php

namespace Drupal\Tests\violinist_teams\Kernel;

use Drupal\node\Entity\Node;
use Drupal\user\Entity\User;
use Drupal\violinist_teams\TeamNode;

/**
 * Tests for the team manager.
 *
 * @group violinist_teams
 */
class TeamManagerTest extends KernelTestBase {

  /**
   * Team manager.
   *
   * @var \Drupal\violinist_teams\TeamManager|object|null
   */
  protected $teamManager;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();
    $this->teamManager = $this->container->get('violinist_teams.team_manager');
  }

  /**
   * Test that we can get teams from user.
   */
  public function testGetTeamsFromUser() {
    /** @var \Drupal\violinist_teams\TeamNode $node */
    $node = Node::create([
      'type' => $this->nodeType->id(),
      'title' => 'test',
    ]);
    $node->save();
    $user = User::create([
      'name' => 'test',
      'mail' => 'test@example.com',
    ]);
    $user->save();
    $node->appendMember($user);
    $node->save();
    $teams = $this->teamManager->getTeamsByUser($user);
    self::assertCount(1, $teams);
    /** @var \Drupal\violinist_teams\TeamNode $team_node_result */
    $team_node_result = reset($teams);
    self::assertEquals($node->id(), $team_node_result->id());
  }

  /**
   * Test that we get the same teams back from every call to the team manager.
   */
  public function testGetSeverealTeamsFromUser() {
    /** @var \Drupal\violinist_teams\TeamNode $node1 */
    $node1 = Node::create([
      'type' => $this->nodeType->id(),
      'title' => 'test1',
    ]);
    /** @var \Drupal\violinist_teams\TeamNode $node2 */
    $node2 = Node::create([
      'type' => $this->nodeType->id(),
      'title' => 'test2',
    ]);
    $user = User::create([
      'name' => 'test',
      'mail' => 'test@example.com',
    ]);
    $user->save();
    $node1->appendMember($user)->save();
    $node2->appendMember($user)->save();
    $teams = $this->teamManager->getTeamsByUser($user);
    self::assertCount(2, $teams);
    $team_ids = array_values(array_map(function (TeamNode $team) {
      return $team->id();
    }, $teams));
    self::assertEquals([$node1->id(), $node2->id()], $team_ids);
  }

  /**
   * Test that we can get teams from user.
   */
  public function testGetTeamsFromUserOnlyAdmin() {
    /** @var \Drupal\violinist_teams\TeamNode $node */
    $node = Node::create([
      'type' => $this->nodeType->id(),
      'title' => 'test',
    ]);
    $node->save();
    $user = User::create([
      'name' => 'test',
      'mail' => 'test@example.com',
    ]);
    $user->save();
    $node->setAdmins([$user]);
    $node->save();
    $teams = $this->teamManager->getTeamsByUser($user);
    self::assertCount(1, $teams);
    /** @var \Drupal\violinist_teams\TeamNode $team_node_result */
    $team_node_result = reset($teams);
    self::assertEquals($node->id(), $team_node_result->id());
  }

  /**
   * Test what happens when we delete a user.
   */
  public function testUserDelete() {
    /** @var \Drupal\violinist_teams\TeamNode $node1 */
    $node1 = Node::create([
      'type' => $this->nodeType->id(),
      'title' => 'test1',
    ]);
    /** @var \Drupal\violinist_teams\TeamNode $node2 */
    $node2 = Node::create([
      'type' => $this->nodeType->id(),
      'title' => 'test2',
    ]);
    $user1 = User::create([
      'name' => 'test1',
      'mail' => 'test1@example.com',
    ]);
    $user1->save();
    $user2 = User::create([
      'name' => 'test2',
      'mail' => 'test2@example.com',
    ]);
    $user2->save();
    // Now let's make the first one with just user 1, and the second with both.
    $node1->appendMember($user1)->save();
    $node2->setMembers([$user1, $user2])->save();
    $node_storage = $this->container->get('entity_type.manager')->getStorage('node');
    $node_storage->resetCache();
    $nodes = $node_storage->loadByProperties([
      'type' => $this->nodeType->id(),
    ]);
    self::assertCount(2, $nodes);
    // Now, let's delete user 1.
    $user1->delete();
    $node_storage->resetCache();
    $nodes = $node_storage->loadByProperties([
      'type' => $this->nodeType->id(),
    ]);
    self::assertCount(1, $nodes);
  }

  /**
   * Test the creation of teams with the manager.
   */
  public function testCreateTeam() {
    $user = User::create([
      'name' => 'test1',
      'mail' => 'test1@example.com',
    ]);
    $user->save();
    // Let's create a team from this user.
    $team = $this->teamManager->createTeam('my team', $user);
    self::assertInstanceOf(TeamNode::class, $team);
    self::assertCount(1, $team->getMembers());
    self::assertEquals([$user->id()], $team->getMemberIds());
    // Also create a team with no user, that should totally be allowed.
    $team = $this->teamManager->createTeam('my team');
    self::assertInstanceOf(TeamNode::class, $team);
    self::assertCount(0, $team->getMembers());
  }

  /**
   * Test the ::getInviteHash function.
   */
  public function testGetInviteHash() {
    $team = Node::create([
      'type' => $this->nodeType->id(),
      'title' => 'test',
    ]);
    $team->save();
    $hash1 = $this->teamManager->getInviteHash($team, 'admin', 123);
    $hash2 = $this->teamManager->getInviteHash($team, 'admin', 123);
    self::assertEquals($hash1, $hash2);
    // Then if we use another role, it should be different.
    $hash3 = $this->teamManager->getInviteHash($team, 'member', 123);
    self::assertNotEquals($hash1, $hash3);
    // And if we use another timestamp, of course it should be different.
    $hash4 = $this->teamManager->getInviteHash($team, 'admin', 124);
    self::assertNotEquals($hash1, $hash4);
    // And most obvious of all. Using another team, totally different.
    $team2 = Node::create([
      'type' => $this->nodeType->id(),
      'title' => 'test2',
    ]);
    $team2->save();
    $hash5 = $this->teamManager->getInviteHash($team2, 'admin', 123);
    self::assertNotEquals($hash1, $hash5);
  }

  /**
   * Test all of the connection setters and getters.
   */
  public function testConnectionMethods() {
    $team = Node::create([
      'type' => $this->nodeType->id(),
      'title' => 'test',
    ]);
    $team->save();
    $this->teamManager->setVcsConnection($team, 'github', 'mytoken');
    self::assertArrayHasKey('github', $this->teamManager->getVcsConnections($team));
    self::assertEquals('mytoken', $this->teamManager->getVcsConnection($team, 'github'));
    $this->teamManager->setVcsConnection($team, 'gitlab', 'mytoken2');
    self::assertArrayHasKey('gitlab', $this->teamManager->getVcsConnections($team));
    self::assertEquals('mytoken2', $this->teamManager->getVcsConnection($team, 'gitlab'));
    // Remove the github one, and ensure its not there any more.
    $this->teamManager->removeVcsConnection($team, 'github');
    self::assertArrayNotHasKey('github', $this->teamManager->getVcsConnections($team));
    // And let's also nuke it entirely by setting it to an empty array. This
    // should remove the gitlab one.
    $this->teamManager->setVcsConnections($team, []);
    self::assertEmpty($this->teamManager->getVcsConnections($team));
  }

}
