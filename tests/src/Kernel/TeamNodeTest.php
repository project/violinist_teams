<?php

namespace Drupal\Tests\violinist_teams\Kernel;

use Drupal\node\Entity\Node;
use Drupal\user\Entity\User;
use Drupal\violinist_teams\PlanInterface;
use Drupal\violinist_teams\TeamNode;

/**
 * Test the project node class.
 *
 * @group violinist_teams
 */
class TeamNodeTest extends KernelTestBase {

  /**
   * Test that we are getting the class we want.
   */
  public function testNodeClass() {
    $node = Node::create([
      'type' => $this->nodeType->id(),
    ]);
    self::assertInstanceOf(TeamNode::class, $node);
  }

  /**
   * Test the ::canViewLogs method.
   */
  public function testLogAccess() {
    /** @var \Drupal\violinist_teams\TeamNode $node */
    $node = Node::create([
      'type' => $this->nodeType->id(),
    ]);
    self::assertFalse($node->canViewLogs());
    $node->setPlan(PlanInterface::AGENCY_PLAN);
    self::assertTrue($node->canViewLogs());
    $node->setPlan(PlanInterface::DEFAULT_PLAN);
    self::assertFalse($node->canViewLogs());
  }

  /**
   * Test append member method.
   */
  public function testAppendMember() {
    /** @var \Drupal\violinist_teams\TeamNode $node */
    $node = Node::create([
      'type' => $this->nodeType->id(),
    ]);
    $user = User::create([
      'name' => 'test',
      'mail' => 'test@example.com',
    ]);
    $user->save();
    $team = $node->appendMember($user);
    self::assertInstanceOf(TeamNode::class, $team);
    self::assertCount(1, $team->getMembers());
  }

  /**
   * Tests getting admins from a node.
   */
  public function testGetAdmins() {
    /** @var \Drupal\violinist_teams\TeamNode $node */
    $node = Node::create([
      'type' => $this->nodeType->id(),
    ]);
    $user = User::create([
      'name' => 'test',
      'mail' => 'test@example.com',
    ]);
    $user->save();
    self::assertEquals([], $node->getAdministrators());
    $node->appendAdmin($user);
    self::assertEquals([$user->id()], $node->getAdministratorIds());
  }

  /**
   * Tests checking admins.
   */
  public function testCheckAdmins() {
    /** @var \Drupal\violinist_teams\TeamNode $node */
    $node = Node::create([
      'type' => $this->nodeType->id(),
    ]);
    $user = User::create([
      'name' => 'test',
      'mail' => 'test@example.com',
    ]);
    $user->save();
    self::assertEquals([], $node->getAdministrators());
    $node->appendAdmin($user);
    self::assertEquals(TRUE, $node->isAdmin($user));
    // Other user, not so much.
    $user2 = User::create([
      'name' => 'test2',
      'mail' => 'test2@example.com',
    ]);
    $user2->save();
    self::assertEquals(FALSE, $node->isAdmin($user2));
  }

  /**
   * Test the member or admin check.
   */
  public function testMemberOrAdmin() {
    /** @var \Drupal\violinist_teams\TeamNode $node */
    $node = Node::create([
      'type' => $this->nodeType->id(),
    ]);
    $user = User::create([
      'name' => 'test',
      'mail' => 'test@example.com',
    ]);
    $user->save();
    self::assertFalse($node->isMemberOrAdminFromUserId($user->id()));
    $node->appendAdmin($user);
    self::assertTrue($node->isMemberOrAdminFromUserId($user->id()));
    // Other user, not so much.
    $user2 = User::create([
      'name' => 'test2',
      'mail' => 'test2@example.com',
    ]);
    $user2->save();
    self::assertFalse($node->isMemberOrAdminFromUserId($user2->id()));
    // As a member, they would though.
    $node->appendMember($user2);
    self::assertTrue($node->isMemberOrAdminFromUserId($user2->id()));
  }

  /**
   * Test the notification variations.
   *
   * @dataProvider notificationProvider
   */
  public function testGetNotifications(string $field_value, array $expexted) {
    /** @var \Drupal\violinist_teams\TeamNode $node */
    $node = Node::create([
      'type' => $this->nodeType->id(),
    ]);
    $node->set('field_team_notification_emails', $field_value);
    self::assertEquals($expexted, $node->getNotificationEmails());
  }

  /**
   * Test the notification variations.
   *
   * @dataProvider notificationProvider
   */
  public function testGetConcurrentNotificationsMails(string $field_value, array $expexted) {
    /** @var \Drupal\violinist_teams\TeamNode $node */
    $node = Node::create([
      'type' => $this->nodeType->id(),
    ]);
    $node->set(TeamNode::NOTIFICATION_MAX_CONCURRENT_EMAILS_FIELD, $field_value);
    self::assertEquals($expexted, $node->getNotiticationEmailsForMaxConcurrent());
  }

  /**
   * Test the notification variations.
   *
   * @dataProvider slackNotificationProvider
   */
  public function testGetSlackNotifications($field_value, bool $expexted) {
    /** @var \Drupal\violinist_teams\TeamNode $node */
    $node = Node::create([
      'type' => $this->nodeType->id(),
    ]);
    $node->set('field_slack_notifications', $field_value);
    self::assertEquals($expexted, $node->isNotificationsEnabledForSlack());
  }

  /**
   * Test for the method to set and get the slack webhook.
   */
  public function testGetSlackWebhook() {
    /** @var \Drupal\violinist_teams\TeamNode $node */
    $node = Node::create([
      'type' => $this->nodeType->id(),
    ]);
    $node->set('field_slack_webhook', 'https://example.com/slack_hook');
    self::assertEquals('https://example.com/slack_hook', $node->getTeamSlackWebhook());
    // Should actually also be possible to use a setter method.
    $node->setTeamSlackWebhook('https://example.com/slack_hook2');
    self::assertEquals('https://example.com/slack_hook2', $node->getTeamSlackWebhook());
  }

  /**
   * Test migrating billing emails.
   *
   * @dataProvider getBillingEmailData
   */
  public function testGetBillingEmails($billing_enabled_value, $billing_email_value, $expected_result) {
    /** @var \Drupal\violinist_teams\TeamNode $node */
    $node = Node::create([
      'type' => $this->nodeType->id(),
    ]);
    $node->set('field_send_receipts', $billing_enabled_value);
    $node->set('field_billing_email', $billing_email_value);
    self::assertEquals($expected_result, $node->getBillingEmails());
  }

  /**
   * Test the environment variables.
   *
   * @dataProvider getEnvironmentVariables
   */
  public function testGetEnvironmentVariables($variable_value, $expected_result) {
    /** @var \Drupal\violinist_teams\TeamNode $node */
    $node = Node::create([
      'type' => $this->nodeType->id(),
    ]);
    $node->set('field_environment_variables', $variable_value);
    self::assertEquals($expected_result, $node->getEnvironmentVariables());
  }

  /**
   * Test logic for getting (and setting) the pull request instructions.
   */
  public function testPullRequestInstructions() {
    // First test an empty one.
    /** @var \Drupal\violinist_teams\TeamNode $node */
    $node = Node::create([
      'type' => $this->nodeType->id(),
    ]);
    self::assertEquals('', $node->getPullRequestInstructions());
    // Now set a value and assume we get it back.
    $node->set('field_pull_request_template', 'This is a test');
    self::assertEquals('This is a test', $node->getPullRequestInstructions());
    // Also make sure the setter work.
    $node->setPullRequestInstructions('This is another test');
    self::assertEquals('This is another test', $node->getPullRequestInstructions());
  }

  /**
   * Test all the things with limits.
   */
  public function testTeamLimits() {
    /** @var \Drupal\violinist_teams\TeamNode $node */
    $node = Node::create([
      'type' => $this->nodeType->id(),
    ]);
    // Default is to have one. When using the plan, that is.
    self::assertEquals(1, $node->getPlan()->getMaxPrivateRepos());
    // The actual limit will be 0 now though.
    self::assertEquals(0, $node->getMaxPrivateRepos());
    self::assertEquals(0, $node->getMaxPrivateReposWithoutOverride());
    // Let's set a value for it.
    $node->setMaxPrivateRepos(2);
    self::assertEquals(2, $node->getMaxPrivateRepos());
    self::assertEquals(2, $node->getMaxPrivateReposWithoutOverride());
    // Still just the one on the plan, since they have not got a plan yet.
    self::assertEquals(1, $node->getPlan()->getMaxPrivateRepos());
    // Now let's set override.
    $node->set('field_team_allowed_override', 3);
    self::assertEquals(3, $node->getMaxPrivateRepos());
    self::assertEquals(2, $node->getMaxPrivateReposWithoutOverride());
    // Still just 1 on the plan.
    self::assertEquals(1, $node->getPlan()->getMaxPrivateRepos());
    // Now let's alter the plan, and make sure the override is used there.
    $node->setPlan(PlanInterface::AGENCY_PLAN);
    self::assertEquals(3, $node->getPlan()->getMaxPrivateRepos());
  }

  /**
   * Test the licence things.
   */
  public function testLicences() {
    /** @var \Drupal\violinist_teams\TeamNode $node */
    $node = Node::create([
      'type' => $this->nodeType->id(),
    ]);
    $node->addLicence('test');
    self::assertEquals(['test'], $node->getLicences());
    // Now add another one, and make sure its there.
    $node->addLicence('test2');
    self::assertEquals(['test', 'test2'], $node->getLicences());
    // Now overwrite it.
    $node->set('field_licences', []);
    $node->addLicence('test3');
    self::assertEquals(['test3'], $node->getLicences());
  }

  /**
   * Dataprovider for environment variables.
   */
  public function getEnvironmentVariables() : array {
    return [
      [
        'FOO=bar',
        ['FOO' => 'bar'],
      ],
      [
        'FOO=bar
        FOO=baz',
        ['FOO' => 'baz'],
      ],
      [
        'FOO=bar
        FOO=baz
        FOO=',
        [],
      ],
      [
        'FOO=
        ',
        [],
      ],
      [
        NULL,
        [],
      ],
      [
        '',
        [],
      ],
      [
        'random_string = no value',
        ['random_string' => 'no value'],
      ],
      [
        'something stupid',
        [],
      ],
      [
        'TEST=maybe_some_key_containing_the_equals_sign6622¤¤==!',
        ['TEST' => 'maybe_some_key_containing_the_equals_sign6622¤¤==!'],
      ],
    ];
  }

  /**
   * Data provider for billing and billing email test.
   */
  public function getBillingEmailData() {
    return [
      [
        NULL,
        NULL,
        [],
      ],
      [
        1,
        NULL,
        [],
      ],
      [
        0,
        'email@emai.com',
        [],
      ],
      [
        TRUE,
        'fox@fbi.gov',
        ['fox@fbi.gov'],
      ],
    ];
  }

  /**
   * Data provider for mail notification thing.
   */
  public function notificationProvider() {
    return [
      [
        'test@test.com',
        ['test@test.com'],
      ],
      [
        ' test@test.com ',
        ['test@test.com'],
      ],
      [
        "test@test.com\ntesttest@test.com ",
        ['test@test.com', 'testtest@test.com'],
      ],
      [
        '',
        [],
      ],
    ];
  }

  /**
   * Data provider for slack notification thing.
   */
  public function slackNotificationProvider() {
    return [
      [
        NULL,
        FALSE,
      ],
      [
        FALSE,
        FALSE,
      ],
      [
        TRUE,
        TRUE,
      ],
      [
        1,
        TRUE,
      ],
    ];
  }

}
