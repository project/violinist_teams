<?php

namespace Drupal\violinist_teams;

/**
 * Describes a plan.
 */
interface PlanInterface {

  const DEFAULT_PLAN = self::REGULAR_PLAN;
  const REGULAR_PLAN = 'plan_regular';
  const TRIAL_PLAN = 'plan_trial';
  const PREMIUM_PLAN = 'plan_premium';
  const AGENCY_PLAN = 'plan_agency';
  const ENTERPRISE_PLAN = 'plan_enterprise';

  const REGULAR_PLAN_MAX = 1;

  const PREMIUM_PLAN_MAX = 5;

  const TRIAL_PLAN_MAX = 5;

  /**
   * Get the max for the plan.
   */
  public function getMaxPrivateRepos() : int;

  /**
   * Gets the id, typcally on of the constants up here.
   */
  public function getId();

}
