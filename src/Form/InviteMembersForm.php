<?php

declare(strict_types=1);

namespace Drupal\violinist_teams\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Url;
use Drupal\violinist_teams\TeamManager;
use Drupal\violinist_teams\TeamNode;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a Violinist teams form.
 */
final class InviteMembersForm extends FormBase {

  /**
   * Team node.
   *
   * @var \Drupal\violinist_teams\TeamNode
   */
  protected $team;

  /**
   * The constructor.
   */
  public function __construct(
    CurrentRouteMatch $routeMatch,
    private TeamManager $teamManager,
    private AccountProxyInterface $currentUser,
  ) {
    $this->routeMatch = $routeMatch;
    $team = $this->routeMatch->getParameter('team');
    if ($team instanceof TeamNode) {
      $this->team = $team;
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('current_route_match'),
      $container->get('violinist_teams.team_manager'),
      $container->get('current_user')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'violinist_teams_team_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, ?TeamNode $team = NULL): array {
    if (!$team instanceof TeamNode) {
      return $form;
    }
    $form['team'] = [
      '#type' => 'value',
      '#value' => $team,
    ];
    $form['description'] = [
      '#prefix' => '<p class="my-2">',
      '#suffix' => '</p>',
      '#markup' => $this->t('Invite users by sharing the one-time use link you can create below. When visiting the link, they will be able to log in with any provider.'),
    ];
    $options = [
      'member' => $this->t('Member'),
      'admin' => $this->t('Administrator'),
    ];
    if (!$team->isAdmin($this->currentUser)) {
      unset($options['admin']);
    }
    $form['role'] = [
      '#type' => 'select',
      '#title' => $this->t('Role'),
      '#options' => $options,
      '#default_value' => 'member',
    ];
    $form['invite_link'] = [
      '#prefix' => '<div id="invite_link" class="hidden">',
      '#suffix' => '</div>',
      '#theme' => 'violinist_teams_invite_link_textfield',
    ];
    $form['cancel'] = [
      '#type' => 'link',
      '#title' => $this->t('Cancel'),
      '#url' => Url::fromRoute('violinist_teams.team_members', [
        'team' => $team->id(),
      ], [
        'attributes' => [
          'class' => [
            'btn',
            'btn-danger',
            'inline-block',
          ],
        ],
      ]),
    ];
    $form['invite'] = [
      '#type' => 'submit',
      '#value' => $this->t('Generate and copy link'),
      '#ajax' => [
        'callback' => '::ajaxSubmit',
      ],
    ];
    return $form;
  }

  /**
   * Ajax callback for the invite.
   */
  public function ajaxSubmit(array &$form, FormStateInterface $form_state) {
    $timestamp = time();
    $team = $form_state->getValue('team');
    $member_type = $form_state->getValue('role');
    $form['invite_link']['#link'] = Url::fromRoute('violinist_teams.invite_accept', [
      'team_id' => $team->id(),
      'timestamp' => time(),
      'membership_type' => $member_type,
      'hash' => $this->teamManager->getInviteHash($team, $member_type, $timestamp),
    ])->setAbsolute()->toString();
    $response = new AjaxResponse();
    $response->addCommand(new ReplaceCommand('#invite_link', $form['invite_link']));
    $response->addCommand(new InvokeCommand('#invite_link', 'show'));
    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
  }

}
