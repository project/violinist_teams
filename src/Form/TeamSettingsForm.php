<?php

declare(strict_types=1);

namespace Drupal\violinist_teams\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Url;
use Drupal\violinist_teams\TeamNode;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a Violinist teams form.
 */
final class TeamSettingsForm extends FormBase {

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected AccountProxyInterface $currentUser;

  /**
   * Team node.
   *
   * @var \Drupal\violinist_teams\TeamNode
   */
  protected TeamNode $team;

  /**
   * The constructor.
   */
  public function __construct(CurrentRouteMatch $current_route_match, AccountProxyInterface $current_user) {
    $this->routeMatch = $current_route_match;
    $this->currentUser = $current_user;
    $team = $this->routeMatch->getParameter('team');
    if ($team instanceof TeamNode) {
      $this->team = $team;
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('current_route_match'),
      $container->get('current_user')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'violinist_teams_team_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $team = $this->team;
    if (!$team instanceof TeamNode) {
      return $form;
    }
    $form['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Team Name'),
      '#required' => TRUE,
      '#default_value' => $team->getTitle(),
    ];
    if (!$team->isAdmin($this->currentUser)) {
      $form['title']['#description'] = $this->t('Only administrators can change the team name.');
      $form['title']['#disabled'] = TRUE;
      $form['title']['#attributes']['readonly'] = 'readonly';
      $form['title']['#attributes']['disabled'] = 'disabled';
    }
    $form['slack'] = [
      '#type' => 'details',
      '#title' => $this->t('Slack notifications'),
      '#open' => TRUE,
    ];
    $form['slack']['enable_slack_notifications'] = [
      '#type' => 'checkbox',
      '#default_value' => $team->isNotificationsEnabledForSlack(),
      '#title' => $this->t('Enable Slack notifications'),
    ];
    if (!$team->getTeamSlackWebhook()) {
      $form['slack']['enable_slack_notifications']['#disabled'] = TRUE;
      $form['slack']['enable_slack_notifications']['#description'] = $this->getSlackDescription();
    }
    $form['email'] = [
      '#type' => 'details',
      '#title' => $this->t('Email notifications'),
      '#open' => TRUE,
    ];
    $form['email']['enable_email_notifications'] = [
      '#type' => 'checkbox',
      '#default_value' => $team->isEmailNotificationsOnFailedUpdatesEnabled(),
      '#description' => $this->t('By checking this box you accept that the emails you provide wants to receive emails from Violinist.io.'),
      '#title' => $this->t('Enable email notifications on failed updates'),
    ];
    $form['email']['notification_emails'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Email addresses to notify'),
      '#default_value' => $team->getNotificationEmailsStringForFailedUpdates(),
      '#description' => $this->t('Please enter a list of email addresses that should receive an email notification, when notifications are sent. Enter one address per line.'),
      '#states' => [
        'visible' => [
          ':input[name="enable_email_notifications"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['email']['enable_email_notifications_concurrent'] = [
      '#type' => 'checkbox',
      '#default_value' => $team->isEmailNotificationsOnMaxConcurrentEnabled(),
      '#description' => $this->t('By checking this box you accept that the emails you provide wants to receive emails from Violinist.io.'),
      '#title' => $this->t('Enable email notifications when reaching max concurrent PRs'),
    ];
    $form['email']['notification_emails_concurrent'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Email addresses to notify'),
      '#default_value' => $team->getNotificationEmailsStringForMaxConcurrent(),
      '#description' => $this->t('Please enter a list of email addresses that should receive an email notification, when notifications are sent. Enter one address per line.'),
      '#states' => [
        'visible' => [
          ':input[name="enable_email_notifications_concurrent"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['environment_vars'] = [
      '#type' => 'details',
      '#title' => $this->t('Environment variables'),
      '#open' => TRUE,
    ];
    $form['environment_vars']['environment_vars'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Environment variables'),
      '#default_value' => $team->getEnvironmentVariables() ? $team->get(TeamNode::ENVIRONMENT_VARIABLES_FIELD)->first()->getString() : '',
      '#description' => $this->t('Please enter a list of environment variables that should be used when running updates.<br><br>If you use project specific variables, these will be merged and the project specific variables will take precedence.<br><br>Enter one variable per line in the format <code>VARIABLE=value</code>.'),
    ];
    $form['pr_instructions'] = [
      '#type' => 'details',
      '#title' => $this->t('Pull request instructions'),
      '#open' => TRUE,
    ];
    $form['pr_instructions']['pr_instructions'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Pull request instructions'),
      '#default_value' => $team->getPullRequestInstructions() ?: '',
      '#description' => $this->t('Specify a message to use on pull requests across projects.<br><br>
      If you specify a value here, this will be used for all pull requests for all of your projects. This message can be overridden per project.'),
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
    ];

    return $form;
  }

  /**
   * Helper to get that description.
   */
  protected function getSlackDescription() {
    return t('You must <a href="@url" class="underline">connect your user to slack</a> to enable this feature', [
      '@url' => Url::fromRoute('league_oauth_login.login_controller_link', [
        'provider_id' => 'slack',
      ], [
        'query' => [
          'team' => $this->team?->id() ?? NULL,
        ],
      ])->toString(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $team = $this->team;
    if (!$team instanceof TeamNode) {
      return;
    }
    if ($this->team->isAdmin($this->currentUser)) {
      $team->setTitle($form_state->getValue('title'));
    }
    $team
      ->set(TeamNode::SLACK_NOTIFICATION_ENABLED_FIELD, $form_state->getValue('enable_slack_notifications'))
      ->set(TeamNode::NOTIFICATION_ENABLED_FIELD, $form_state->getValue('enable_email_notifications'))
      ->set(TeamNode::NOTIFICATION_EMAILS_FIELD, $form_state->getValue('notification_emails'))
      ->set(TeamNode::NOTIFICATION_MAX_CONCURRENT_ENABLED_FIELD, $form_state->getValue('enable_email_notifications_concurrent'))
      ->set(TeamNode::NOTIFICATION_MAX_CONCURRENT_EMAILS_FIELD, $form_state->getValue('notification_emails_concurrent'))
      ->set(TeamNode::ENVIRONMENT_VARIABLES_FIELD, $form_state->getValue('environment_vars'))
      ->set(TeamNode::PR_INSTRUCTIONS_FIELD, $form_state->getValue('pr_instructions'))
      ->save();
  }

}
