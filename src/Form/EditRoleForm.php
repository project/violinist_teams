<?php

declare(strict_types=1);

namespace Drupal\violinist_teams\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\UserInterface;
use Drupal\violinist_teams\TeamNode;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a Violinist teams form.
 */
final class EditRoleForm extends FormBase {

  public function __construct(
    private EntityTypeManagerInterface $entityTypeManager,
  ) {}

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'violinist_teams_edit_role';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, ?TeamNode $team = NULL, $uid = NULL): array {
    if (!$team instanceof TeamNode || !$uid) {
      return $form;
    }

    // Let's find out if this uid is even in the team.
    if (!in_array($uid, $team->getMemberIds()) && !in_array($uid, $team->getAdministratorIds())) {
      return $form;
    }

    $form['team'] = [
      '#type' => 'value',
      '#value' => $team,
    ];

    $form['uid'] = [
      '#type' => 'value',
      '#value' => $uid,
    ];

    $options = [
      'member' => $this->t('Member'),
      'admin' => $this->t('Administrator'),
    ];

    $default_value = 'member';
    if (in_array($uid, $team->getAdministratorIds())) {
      $default_value = 'admin';
    }

    $form['role'] = [
      '#type' => 'select',
      '#title' => $this->t('Role'),
      '#options' => $options,
      '#default_value' => $default_value,
    ];

    $form['actions'] = [
      '#type' => 'actions',
      'submit' => [
        '#type' => 'submit',
        '#value' => $this->t('Save'),
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    $team = $form_state->getValue('team');
    if (!$team instanceof TeamNode) {
      $form_state->setErrorByName('team', $this->t('Invalid team'));
    }
    $uid = $form_state->getValue('uid');
    $user = $this->entityTypeManager->getStorage('user')->load($uid);
    if (!$user instanceof UserInterface) {
      $form_state->setErrorByName('uid', $this->t('Invalid user'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    // Find the new role they should have.
    $role = $form_state->getValue('role');
    $team = $form_state->getValue('team');
    if (!$team instanceof TeamNode) {
      // Not sure how that would be remotely possible.
      return;
    }
    $uid = $form_state->getValue('uid');
    $user = $this->entityTypeManager->getStorage('user')->load($uid);
    if (!$user instanceof UserInterface) {
      // Not sure how that would be remotely possible.
      return;
    }
    if ($role === 'admin') {
      // It's fine if they are both a memeber and an admin.
      $team->appendAdmin($user);
    }
    else {
      // Make sure we remove their admin role, should they have one.
      $admins = $team->getAdministrators();
      $admins = array_filter($admins, function (UserInterface $admin) use ($user) {
        return $admin->id() !== $user->id();
      });
      $team->setAdmins($admins);
      $team->appendMember($user);
    }
    $team->save();
    $form_state->setRedirect('violinist_teams.team_members', ['team' => $form_state->getValue('team')->id()]);
  }

}
