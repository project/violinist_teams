<?php

declare(strict_types=1);

namespace Drupal\violinist_teams\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Drupal\violinist_teams\TeamNode;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The team delete form.
 */
final class TeamDeleteConfirmForm extends ConfirmFormBase {

  /**
   * Team node.
   *
   * @var \Drupal\violinist_teams\TeamNode
   */
  protected TeamNode $team;

  /**
   * Construct the delete form.
   */
  public function __construct(RouteMatchInterface $route_match, MessengerInterface $messenger) {
    $this->routeMatch = $route_match;
    $this->team = $route_match->getParameter('team');
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('current_route_match'),
      $container->get('messenger')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'violinist_teams_team_delete_confirm';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion(): TranslatableMarkup {
    return $this->t('Are you sure you want to delete team @team?', [
      '@team' => $this->team->label(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->t('This will delete the team and all associated data, including all projects monitored and team memberships. This action is permanent and cannot be undone');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl(): Url {
    return $this->team->toUrl();
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->team->delete();
    $this->messenger->addStatus($this->t('Team @team has been deleted.', [
      '@team' => $this->team->label(),
    ]));
    $form_state->setRedirectUrl(new Url('user.page'));
  }

}
