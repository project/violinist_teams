<?php

declare(strict_types=1);

namespace Drupal\violinist_teams\EventSubscriber;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\user\UserInterface;
use Drupal\violinist_teams\Controller\InviteController;
use Drupal\violinist_teams\TeamNode;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * A response subscriber to check if we have outstanding invites.
 */
final class ResponseInviteSubscriber implements EventSubscriberInterface {

  /**
   * Constructs a ResponseInviteSubscriber object.
   */
  public function __construct(
    private readonly SessionInterface $session,
    private readonly EntityTypeManagerInterface $entityTypeManager,
    private AccountProxyInterface $currentUser,
  ) {}

  /**
   * Kernel response event handler.
   */
  public function onKernelResponse(ResponseEvent $event): void {
    if (!$this->currentUser->id()) {
      return;
    }
    $request = $event->getRequest();
    $data = $request->getSession()->get(InviteController::INVITE_DATA);
    if (!$data) {
      return;
    }
    // No matter what the result is here, we want to remove the session data at
    // this point.
    $request->getSession()->remove(InviteController::INVITE_DATA);
    if (empty($data['team_id'])) {
      return;
    }
    if (empty($data['membership_type'])) {
      return;
    }
    $team = $this->entityTypeManager->getStorage('node')->load($data['team_id']);
    if (!$team instanceof TeamNode) {
      return;
    }
    $actual_user_object = $this->entityTypeManager->getStorage('user')->load($this->currentUser->id());
    if (!$actual_user_object instanceof UserInterface) {
      return;
    }
    $membership_type = $data['membership_type'];
    // Add them to the team. Based on what role we have in the parameter.
    if ($membership_type === 'admin') {
      $team->appendAdmin($actual_user_object)->save();
    }
    else {
      $team->appendMember($actual_user_object)->save();
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      KernelEvents::RESPONSE => ['onKernelResponse'],
    ];
  }

}
