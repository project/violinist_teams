<?php

namespace Drupal\violinist_teams\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\violinist_teams\TeamNode;
use Symfony\Component\Routing\Route;

/**
 * Access service for team access.
 */
final class TeamAccess implements AccessInterface {

  public function __construct(
    private AccountProxyInterface $currentUser,
  ) {}

  /**
   * Controls the access for this service.
   */
  public function access(Route $route, RouteMatchInterface $route_match) : AccessResult {
    $access_type = $route->getRequirement('_violinist_teams_access');
    $result = AccessResult::forbidden();
    $team = $route_match->getParameter('team');
    if (!$team instanceof TeamNode) {
      return AccessResult::neutral('No team node found');
    }
    if ($this->currentUser->hasPermission('administer all teams')) {
      return AccessResult::allowed();
    }
    // @todo In theory we could add more types here. For example, it could
    // theoretically be wanted to show something to members only, but not to
    // admins. Currently that is not really needed though.
    switch ($access_type) {
      case 'admin':
        $result = AccessResult::allowedIf($team->isAdmin($this->currentUser));
        break;

      default:
        // The default is to only allow if you are part of the team. Either as a
        // member or as an admin.
        $result = AccessResult::allowedIf($team->isMemberOrAdminFromUserId($this->currentUser->id()));
        break;
    }
    return $result;
  }

}
