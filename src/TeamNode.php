<?php

namespace Drupal\violinist_teams;

use Drupal\Core\Session\AccountInterface;
use Drupal\node\Entity\Node;
use Drupal\user\UserInterface;

/**
 * A bundle class for our teams.
 */
class TeamNode extends Node {

  const NODE_TYPE = 'team';
  const MEMBERS_FIELD = 'field_team_members';
  const ADMIN_FIELD = 'field_team_admins';
  const PLAN_FIELD = 'field_plan';
  const PROJECT_LIMIT_FIELD = 'field_project_limit';
  const PROJECT_LIMIT_OVERRIDE_FIELD = 'field_team_allowed_override';
  const SLACK_NOTIFICATION_ENABLED_FIELD = 'field_slack_notifications';
  const NOTIFICATION_ENABLED_FIELD = 'field_notifications_on_failed';
  const NOTIFICATION_EMAILS_FIELD = 'field_team_notification_emails';
  const NOTIFICATION_MAX_CONCURRENT_ENABLED_FIELD = 'field_max_concurrent_status';
  const NOTIFICATION_MAX_CONCURRENT_EMAILS_FIELD = 'field_max_concurrent_emails';
  const BILLING_ENABLED_FIELD = 'field_send_receipts';
  const BILLING_EMAIL_FIELD = 'field_billing_email';
  const ENVIRONMENT_VARIABLES_FIELD = 'field_environment_variables';
  const PR_INSTRUCTIONS_FIELD = 'field_pull_request_template';

  /**
   * Helper to return if the team has access or not.
   */
  public function canViewLogs() : bool {
    $plan_id = $this->getPlan()->getId();
    $allowed_to_view_logs = [
      PlanInterface::TRIAL_PLAN,
      PlanInterface::PREMIUM_PLAN,
      PlanInterface::AGENCY_PLAN,
      PlanInterface::ENTERPRISE_PLAN,
    ];
    return in_array($plan_id, $allowed_to_view_logs);
  }

  /**
   * Get the env variables.
   *
   * Returns an array of env variables, with the key being the name of the
   * variable, and the value being the value. This, despite the fact that it is
   * stored as a single textfield, which in theory would make the user able to
   * override their own variables within the same form. This would then be
   * considered a feature.
   */
  public function getEnvironmentVariables() : array {
    $variables = [];
    if (!$this->hasField(self::ENVIRONMENT_VARIABLES_FIELD) || $this->get(self::ENVIRONMENT_VARIABLES_FIELD)->isEmpty()) {
      return $variables;
    }
    $variables_string = $this->get(self::ENVIRONMENT_VARIABLES_FIELD)->first()->getString();
    $variables_string = trim($variables_string);
    if (!$variables_string) {
      return $variables;
    }
    $variables_array = preg_split('/\r\n|\r|\n/', $variables_string);
    foreach ($variables_array as $variable) {
      $variable = trim($variable);
      if (!$variable) {
        continue;
      }
      $variable_parts = explode('=', $variable);
      if (count($variable_parts) < 2) {
        // Not really looking like a good env var.
        continue;
      }
      $key = trim($variable_parts[0]);
      $value = trim($variable_parts[1]);
      if (count($variable_parts) !== 2) {
        // Join all the others so that the value can in fact contain an =.
        array_shift($variable_parts);
        $variable_parts = array_map('trim', $variable_parts);
        $value = implode('=', $variable_parts);
      }
      $variables[$key] = $value;
    }
    $variables = array_filter($variables);
    return $variables;
  }

  /**
   * Currently this is only one email, but we return an array for consistency.
   *
   * And I guess for future proofing.
   */
  public function getBillingEmails() : array {
    if (!$this->hasField(self::BILLING_ENABLED_FIELD) || $this->get(self::BILLING_ENABLED_FIELD)->isEmpty()) {
      return [];
    }
    $enabled = (bool) $this->get(self::BILLING_ENABLED_FIELD)->first()->getString();
    if (!$enabled) {
      return [];
    }
    if (!$this->hasField(self::BILLING_EMAIL_FIELD) || $this->get(self::BILLING_EMAIL_FIELD)->isEmpty()) {
      return [];
    }
    $email = $this->get(self::BILLING_EMAIL_FIELD)->first()->getString();
    return [$email];
  }

  /**
   * Are those slack notifications enabled, globally, for the team?
   */
  public function isNotificationsEnabledForSlack() : bool {
    if (!$this->hasField(self::SLACK_NOTIFICATION_ENABLED_FIELD) || $this->get(self::SLACK_NOTIFICATION_ENABLED_FIELD)->isEmpty()) {
      // No way to see what sort of settings they have.
      return FALSE;
    }
    return (bool) $this->get(self::SLACK_NOTIFICATION_ENABLED_FIELD)->first()->getString();
  }

  /**
   * Get the emails from the field.
   */
  public function getNotificationEmails() {
    return self::createMailArrayFromString($this->getNotificationEmailsStringForFailedUpdates());
  }

  /**
   * Get the emails from the max concurrent email field.
   */
  public function getNotiticationEmailsForMaxConcurrent() {
    return self::createMailArrayFromString($this->getNotificationEmailsStringForMaxConcurrent());
  }

  /**
   * Return the actual string stored.
   *
   * This is useful to fill out a default value in a form, for example.
   */
  public function getNotificationEmailsStringForFailedUpdates() {
    $mail_string = '';
    if (!$this->hasField('field_team_notification_emails') || $this->get('field_team_notification_emails')->isEmpty()) {
      return $mail_string;
    }
    $mail_string = $this->get('field_team_notification_emails')->first()->getString();
    return $mail_string;
  }

  /**
   * Return the actual string stored.
   *
   * This is useful to fill out a default value in a form, for example.
   */
  public function getNotificationEmailsStringForMaxConcurrent() {
    $mail_string = '';
    if (!$this->hasField(self::NOTIFICATION_MAX_CONCURRENT_EMAILS_FIELD) || $this->get(self::NOTIFICATION_MAX_CONCURRENT_EMAILS_FIELD)->isEmpty()) {
      return $mail_string;
    }
    $mail_string = $this->get(self::NOTIFICATION_MAX_CONCURRENT_EMAILS_FIELD)->first()->getString();
    return $mail_string;
  }

  /**
   * Helper for the status on the max concurrent email.
   */
  public function isEmailNotificationsOnMaxConcurrentEnabled() {
    if (!$this->hasField(self::NOTIFICATION_MAX_CONCURRENT_ENABLED_FIELD) || $this->get(self::NOTIFICATION_MAX_CONCURRENT_ENABLED_FIELD)->isEmpty()) {
      return FALSE;
    }
    return (bool) $this->get(self::NOTIFICATION_MAX_CONCURRENT_ENABLED_FIELD)->first()->getString();
  }

  /**
   * Whether notifications on failed updates are enabled or not.
   */
  public function isEmailNotificationsOnFailedUpdatesEnabled() : bool {
    if (!$this->hasField(self::NOTIFICATION_ENABLED_FIELD) || $this->get(self::NOTIFICATION_ENABLED_FIELD)->isEmpty()) {
      return FALSE;
    }
    return (bool) $this->get(self::NOTIFICATION_ENABLED_FIELD)->first()->getString();
  }

  /**
   * Helper to make sure we only get somehow valid strings at least.
   */
  protected static function createMailArrayFromString(string $mails_string) : array {
    $mails = explode("\n", $mails_string);
    $mails = array_map('trim', $mails);
    return array_filter($mails);
  }

  /**
   * Get the max private repos a team can have, based on the field value.
   */
  public function getMaxPrivateRepos() : int {
    $return = $this->getMaxPrivateReposWithoutOverride();
    if ($this->hasProjectOverrideLimit()) {
      $return = $this->getProjectOverrideLimit();
    }
    return $return;
  }

  /**
   * Get the max private repos a team can have, based on the field value.
   *
   * This will discard the override field.
   */
  public function getMaxPrivateReposWithoutOverride() : int {
    $return = 0;
    if (!$this->hasField(self::PROJECT_LIMIT_FIELD) || $this->get(self::PROJECT_LIMIT_FIELD)->isEmpty()) {
      return $return;
    }
    $return = (int) $this->get(self::PROJECT_LIMIT_FIELD)->first()->getString();
    return $return;
  }

  /**
   * Check if the team has a project override limit.
   */
  public function hasProjectOverrideLimit() : bool {
    if (!$this->hasField(self::PROJECT_LIMIT_OVERRIDE_FIELD) || $this->get(self::PROJECT_LIMIT_OVERRIDE_FIELD)->isEmpty()) {
      return FALSE;
    }
    return (bool) $this->get(self::PROJECT_LIMIT_OVERRIDE_FIELD)->first()->getString();
  }

  /**
   * Get the project override limit.
   */
  public function getProjectOverrideLimit() : int {
    if (!$this->hasProjectOverrideLimit()) {
      return 0;
    }
    return (int) $this->get(self::PROJECT_LIMIT_OVERRIDE_FIELD)->first()->getString();
  }

  /**
   * Setter for max private repos.
   */
  public function setMaxPrivateRepos($number) {
    if (!$this->hasField(self::PROJECT_LIMIT_FIELD)) {
      return;
    }
    $this->set(self::PROJECT_LIMIT_FIELD, $number);
  }

  /**
   * Setter for max private repos.
   */
  public function setMaxPrivateReposOverride($number) {
    if (!$this->hasField(self::PROJECT_LIMIT_OVERRIDE_FIELD)) {
      return;
    }
    $this->set(self::PROJECT_LIMIT_OVERRIDE_FIELD, $number);
  }

  /**
   * Setter for the plan.
   */
  public function setPlan($plan) : void {
    if (!$this->hasField(self::PLAN_FIELD)) {
      throw new \RuntimeException('The plan field is not present on the team node');
    }
    // @todo Some validation of what plans we are setting, probably?
    $this->get(self::PLAN_FIELD)->setValue($plan);
  }

  /**
   * Get the team plan.
   */
  public function getPlan() : PlanInterface {
    $default = new Plan(PlanInterface::DEFAULT_PLAN);
    if (!$this->hasField('field_plan') || $this->get('field_plan')->isEmpty()) {
      return $default;
    }
    $plan_value = $this->get('field_plan')->first()->getString();
    switch ($plan_value) {
      case PlanInterface::PREMIUM_PLAN:
      case PlanInterface::TRIAL_PLAN:
        return new Plan($plan_value);

      case PlanInterface::AGENCY_PLAN:
      case PlanInterface::ENTERPRISE_PLAN:
        $plan = new Plan($plan_value);
        // Usually we are required to set what the max is on the team level.
        $plan->setMaxPrivateRepos($this->getMaxPrivateRepos());
        return $plan;

      default:
        return $default;
    }
  }

  /**
   * Check if a user is an admin of the team.
   */
  public function isAdmin(AccountInterface $user) {
    if (!$user->id()) {
      return FALSE;
    }
    return in_array($user->id(), $this->getAdministratorIds());
  }

  /**
   * Getter for this field.
   */
  public function getPullRequestInstructions() : ?string {
    if (!$this->hasField(self::PR_INSTRUCTIONS_FIELD) || $this->get(self::PR_INSTRUCTIONS_FIELD)->isEmpty()) {
      return FALSE;
    }
    return $this->get(self::PR_INSTRUCTIONS_FIELD)->first()->getString();
  }

  /**
   * Setter for this field.
   */
  public function setPullRequestInstructions(string $instructions) {
    if (!$this->hasField('field_pull_request_template')) {
      return;
    }
    $this->set('field_pull_request_template', $instructions);
  }

  /**
   * Get all of the admins of a team.
   *
   * Will return an array of user ids.
   */
  public function getAdministratorIds() : array {
    if (!$this->hasField(self::ADMIN_FIELD) || $this->get(self::ADMIN_FIELD)->isEmpty()) {
      return [];
    }
    return array_map(function ($item) {
      return $item["target_id"];
    }, $this->get(self::ADMIN_FIELD)->getValue());
  }

  /**
   * Get all of the admins of a team, the actual user objects.
   */
  public function getAdministrators() : array {
    if (!$this->hasField(self::ADMIN_FIELD) || $this->get(self::ADMIN_FIELD)->isEmpty()) {
      return [];
    }
    return $this->get(self::ADMIN_FIELD)->referencedEntities();
  }

  /**
   * Append an admin.
   *
   * If you are trying to append a user that is already in there, it will only
   * be referenced once.
   */
  public function appendAdmin(UserInterface $user) : self {
    if (!$user->id()) {
      throw new \InvalidArgumentException('Can not reference user id 0 when adding admins');
    }
    $admins = $this->getAdministrators();
    $already_has_it = (bool) array_filter($admins, function (UserInterface $user_item) use ($user) {
      return $user->id() === $user_item->id();
    });
    if ($already_has_it) {
      return $this;
    }
    $admins[] = $user;
    $this->setAdmins($admins);
    // If the user is not also a member, let's add them as a member as well.
    $member_ids = $this->getMemberIds();
    $already_added_as_member = array_filter($member_ids, function ($id) use ($user) {
      return $id === $user->id();
    });
    if (!$already_added_as_member) {
      $this->appendMember($user);
    }
    return $this;
  }

  /**
   * Append a member.
   *
   * If you are trying to append a user that is already in there, it will only
   * be referenced once.
   */
  public function appendMember(UserInterface $user) : self {
    if (!$user->id()) {
      throw new \InvalidArgumentException('Can not reference user id 0 when setting members');
    }
    $members = $this->getMembers();
    $already_has_it = (bool) array_filter($members, function (UserInterface $user_item) use ($user) {
      return $user->id() === $user_item->id();
    });
    if ($already_has_it) {
      return $this;
    }
    $members[] = $user;
    $this->setMembers($members);
    return $this;
  }

  /**
   * Set all of the members of the node.
   */
  public function setMembers(array $members) : self {
    $this->get(self::MEMBERS_FIELD)
      ->setValue($members);
    return $this;
  }

  /**
   * Set all of the admins of the node.
   */
  public function setAdmins(array $admins) : self {
    $this->get(self::ADMIN_FIELD)
      ->setValue($admins);
    return $this;
  }

  /**
   * Helper to get all members of this team.
   *
   * Basically a wrapper around ::referencedEntities but this is the beauty of
   * entity bundle classes. They can have actual descriptive names.
   *
   * Please remember that this will only get actual loadable users, and not
   * deleted ones, for example. Or about to be deleted.
   *
   * @return \Drupal\user\UserInterface[]
   *   All of those users.
   */
  public function getMembers() : array {
    return $this->get(self::MEMBERS_FIELD)->referencedEntities();
  }

  /**
   * Get the IDs of members in the team.
   *
   * This is useful so you can actually get ids of even deleted users.
   */
  public function getMemberIds() : array {
    return array_map(function ($item) {
      return $item['target_id'];
    }, $this->get(self::MEMBERS_FIELD)->getValue());
  }

  /**
   * Based on a user id, is the user a member or an admin.
   */
  public function isMemberOrAdminFromUserId($user_id) : bool {
    return in_array($user_id, $this->getMemberIds()) || in_array($user_id, $this->getAdministratorIds());
  }

  /**
   * Getter for the slack webhook.
   */
  public function getTeamSlackWebhook() {
    if (!$this->hasField('field_slack_webhook') || $this->get('field_slack_webhook')->isEmpty()) {
      return FALSE;
    }
    return $this->get('field_slack_webhook')->first()->getString();
  }

  /**
   * Setter for the slack webhook.
   */
  public function setTeamSlackWebhook($webhook) : self {
    if (!$this->hasField('field_slack_webhook')) {
      return $this;
    }
    $this->set('field_slack_webhook', $webhook);
    return $this;
  }

  /**
   * Add a licence to the team.
   */
  public function addLicence(string $licence) {
    $this->get('field_licences')->appendItem($licence);
    return $this;
  }

  /**
   * Get the actual licences a team has saved.
   */
  public function getLicences() : array {
    if (!$this->hasField('field_licences') || $this->get('field_licences')->isEmpty()) {
      return [];
    }
    return array_map(function ($item) {
      return $item['value'];
    }, $this->get('field_licences')->getValue());
  }

}
