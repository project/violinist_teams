<?php

namespace Drupal\violinist_teams\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\violinist_teams\TeamManager;
use Drupal\violinist_teams\TeamNode;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Returns responses for Violinist teams routes.
 */
class TeamProjectController extends ControllerBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Team manager.
   *
   * @var \Drupal\violinist_teams\TeamManager
   */
  protected $teamManager;

  /**
   * The controller constructor.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, TeamManager $teamManager) {
    $this->entityTypeManager = $entity_type_manager;
    $this->teamManager = $teamManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('violinist_teams.team_manager')
    );
  }

  /**
   * Builds the response.
   */
  public function build(TeamNode $team) {
    $projects = $this->teamManager->getProjects($team);
    $build = [
      '#theme' => 'violinist_teams_projects',
      '#projects' => $projects,
      '#team' => $team,
    ];
    return $build;
  }

}
