<?php

declare(strict_types=1);

namespace Drupal\violinist_teams\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\user\UserInterface;
use Drupal\violinist_teams\TeamManager;
use Drupal\violinist_teams\TeamNode;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Returns responses for Violinist teams routes.
 */
final class InviteController extends ControllerBase {

  const INVITE_DATA = 'violinist_teams_invite_data';

  public function __construct(
    private TeamManager $teamManager,
    EntityTypeManagerInterface $entityTypeManager,
    AccountProxyInterface $currentUser,
    ModuleHandlerInterface $moduleHandler,
    private SessionInterface $session,
  ) {
    $this->entityTypeManager = $entityTypeManager;
    $this->currentUser = $currentUser;
    $this->moduleHandler = $moduleHandler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('violinist_teams.team_manager'),
      $container->get('entity_type.manager'),
      $container->get('current_user'),
      $container->get('module_handler'),
      $container->get('session')
    );
  }

  /**
   * Builds the response.
   */
  public function __invoke($team_id, $membership_type, $timestamp, $hash) {
    // First check that this is in fact an actual team.
    $team = $this->entityTypeManager->getStorage('node')->load($team_id);
    if (!$team instanceof TeamNode) {
      throw new AccessDeniedHttpException('No team loaded from id ' . $team_id);
    }
    // Then validate the hash.
    $calculated_hash = $this->teamManager->getInviteHash($team, $membership_type, (int) $timestamp);
    if ($calculated_hash != $hash) {
      throw new AccessDeniedHttpException('Invalid hash');
    }
    // Now have a look if the user is logged in.
    if ($this->currentUser->isAuthenticated()) {
      $user = $this->entityTypeManager->getStorage('user')->load($this->currentUser->id());
      if (!$user instanceof UserInterface) {
        throw new AccessDeniedHttpException('No user loaded from id ' . $this->currentUser->id());
      }
      // Add them to the team. Based on what role we have in the parameter.
      if ($membership_type === 'admin') {
        $team->appendAdmin($user)->save();
      }
      else {
        $team->appendMember($user)->save();
      }
      // Then let's just redirect to the team, shall we?
      $redirect_response = $this->redirect('entity.node.canonical', ['node' => $team->id()]);
      $this->moduleHandler->alter('violinist_teams_invite_redirect', $redirect_response, $team, $user);
      return $redirect_response;
    }
    // OK so the user is not authenticated. Let's redirect them to the login
    // page. But first, let's set a session cookie so we can actually do the
    // assigning when the user returns.
    $this->session->set(self::INVITE_DATA, [
      'team_id' => $team_id,
      'membership_type' => $membership_type,
    ]);
    $redirect_response = $this->redirect('user.login');
    $this->moduleHandler->alter('violinist_teams_invite_anonymous_redirect', $redirect_response);
    return $redirect_response;
  }

}
