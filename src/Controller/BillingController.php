<?php

namespace Drupal\violinist_teams\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\violinist_teams\PlanInterface;
use Drupal\violinist_teams\TeamNode;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Returns responses for Violinist teams routes.
 */
class BillingController extends ControllerBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The controller constructor.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, ModuleHandlerInterface $module_handler) {
    $this->entityTypeManager = $entity_type_manager;
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('module_handler')
    );
  }

  /**
   * Builds the response.
   */
  public function build(TeamNode $team) {
    $can_upgrade = TRUE;
    $plan_name = t('Free');
    $plan = $team->getPlan();
    $plan_id = $plan->getId();
    if ($plan_id === PlanInterface::PREMIUM_PLAN) {
      $can_upgrade = FALSE;
      $plan_name = t('Paid');
    }
    if ($plan_id === PlanInterface::AGENCY_PLAN) {
      $can_upgrade = FALSE;
      $plan_name = t('Agency');
    }
    if ($plan_id === PlanInterface::ENTERPRISE_PLAN) {
      $can_upgrade = FALSE;
      $plan_name = t('Enterprise');
    }
    $change_plan_txt = t('<p>If you want to change your plan (upgrade to the agency plan, change your number of projects or cancel your plan), please use the "Manage my plan" button above.</p>');
    if ($plan_id === 'agency') {
      $change_plan_txt = t('<p>If you want to change your plan (upgrade to the enterprise plan, change your number of projects or cancel your plan), please use the "Manage my plan" button above.</p>');
    }
    if ($plan_id === 'enterprise') {
      $change_plan_txt = t('<p>If you want to change your plan (change your number of projects or cancel your plan), please use the "Manage my plan" button above.</p>');
    }

    $plan_build = [
      'heading' => [
        '#weight' => -100,
        '#markup' => '<h1>' . t('Your plan') . '</h1>',
      ],
      'description' => [
        '#weight' => -90,
        '#markup' => '<p class="plan-paragraph">' . t('You are on the @plan plan.', [
          '@plan' => $plan_name,
        ]) . '</p>',
      ],
      'extra_info' => [
        '#weight' => -80,
        '#access' => !$can_upgrade,
        '#markup' => $change_plan_txt,
      ],
      'upgrade' => [
        '#weight' => -70,
        '#access' => $can_upgrade,
        '#markup' => '<p class="my-4">' . Link::fromTextAndUrl(t('Upgrade your plan / redeem a coupon'), Url::fromUserInput('/upgrade', [
          'attributes' => [
            'class' => [
              'btn',
              'btn-primary',
            ],
          ],
          'query' => [
            'team' => $team->id(),
          ],
        ]))->toString() . '</p>',
      ],
    ];
    $this->moduleHandler->alter('violinist_teams_billing_build', $plan_build, $team, $can_upgrade);
    return [$plan_build];
  }

}
