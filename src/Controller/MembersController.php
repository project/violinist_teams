<?php

namespace Drupal\violinist_teams\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Url;
use Drupal\violinist_teams\TeamNode;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Controller for the team members page.
 */
final class MembersController extends ControllerBase {

  /**
   * The actual constructor for this controller.
   */
  public function __construct(AccountProxyInterface $currentUser) {
    $this->currentUser = $currentUser;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('current_user')
    );
  }

  /**
   * Build the team members page.
   */
  public function build(TeamNode $team) {
    $build = [];
    $build['invite'] = [
      '#theme' => 'violinist_teams_invite_link_wrapper',
      '#link_element' => [
        '#theme' => 'violinist_teams_invite_link',
        '#link_href' => Url::fromRoute('violinist_teams.team_members_invite', ['team' => $team->id()])->toString(),
        '#invite_access' => $team->isAdmin($this->currentUser),
      ],
    ];
    // Then let's also show a table of all the users, and the status they have.
    $admins = $team->getAdministrators();
    $members = $team->getMembers();
    $rows = [];
    $ids_added = [];
    foreach ($admins as $admin) {
      $ids_added[] = $admin->id();
      $rows[] = [
        '#theme' => 'violinist_teams_team_member_row',
        '#user' => $admin,
        '#role' => $this->t('Administrator'),
        '#team' => $team,
      ];
    }
    foreach ($members as $member) {
      if (in_array($member->id(), $ids_added)) {
        continue;
      }
      $rows[] = [
        '#theme' => 'violinist_teams_team_member_row',
        '#user' => $member,
        '#role' => $this->t('Member'),
        '#team' => $team,
      ];
    }

    $build['members'] = [
      '#theme' => 'violinist_teams_team_members_table',
      '#rows' => $rows,
      '#has_edit' => $team->isAdmin($this->currentUser),
    ];
    return $build;
  }

}
