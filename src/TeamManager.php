<?php

namespace Drupal\violinist_teams;

use Drupal\Component\Utility\Crypt;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Site\Settings;
use Drupal\Core\State\State;
use Drupal\user\UserInterface;
use League\OAuth2\Client\Token\AccessToken;
use Psr\Log\LoggerInterface;

/**
 * Service for all things with managing teams I guess?
 */
class TeamManager {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * State service.
   *
   * @var \Drupal\Core\State\State
   */
  protected $state;

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected LoggerInterface $logger;

  /**
   * Constructs a TeamManager object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\State\State $state
   *   The state service.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, State $state, LoggerInterface $logger) {
    $this->entityTypeManager = $entity_type_manager;
    $this->state = $state;
    $this->logger = $logger;
  }

  /**
   * Get all the projects of a team.
   *
   * @return \Drupal\violinist_projects\ProjectNode[]
   *   An array of project nodes.
   */
  public function getProjects(TeamNode $node) : array {
    $storage = $this->entityTypeManager->getStorage('node');
    $nids = $storage
      ->getQuery()
      // @todo We would have used the constants from the projects module here,
      // but we can not depend on that module since that would create a circular
      // dependency, really.
      ->accessCheck(TRUE)
      ->condition('type', 'project')
      ->condition('field_team', $node->id())
      ->execute();
    return $storage->loadMultiple($nids);
  }

  /**
   * Gets all the teams for a given user.
   *
   * @return \Drupal\violinist_teams\TeamNode[]
   *   An array of teams the user belong to. Or, if none, an empty array of
   *   course.
   */
  public function getTeamsByUser(AccountInterface $user) : array {
    $node_storage = $this->entityTypeManager->getStorage('node');
    $nids_q = $node_storage
      ->getQuery()
      ->accessCheck(FALSE)
      ->condition('type', TeamNode::NODE_TYPE);

    $or = $nids_q->orConditionGroup()
      ->condition('field_team_members', $user->id())
      ->condition('field_team_admins', $user->id());
    $nids_q->condition($or);

    // Make sure they are somewhat consistent.
    $nids_q->sort('nid');

    $nids = $nids_q->execute();
    return $node_storage->loadMultiple($nids);
  }

  /**
   * Create a team, optionally with the first member of the team.
   */
  public function createTeam(string $name, ?UserInterface $user = NULL) : TeamNode {
    /** @var \Drupal\violinist_teams\TeamNode $team */
    $team = $this->entityTypeManager->getStorage('node')
      ->create([
        'type' => TeamNode::NODE_TYPE,
        'title' => $name,
      ]);
    if ($user) {
      $team->appendMember($user);
    }
    $team->save();
    return $team;
  }

  /**
   * Handles user deletions.
   */
  public function handleUserDelete(UserInterface $user) {
    $teams = $this->getTeamsByUser($user);
    if (empty($teams)) {
      // Nothing to do here.
      return;
    }
    foreach ($teams as $team) {
      foreach ($team->getMemberIds() as $member) {
        if ($member == $user->id()) {
          continue;
        }
        // If it's not the same id, let's not delete this team.
        continue 2;
      }
      $this->logger->info('Deleting team @team because user @user was the last member.', [
        '@team' => $team->label(),
        '@user' => $user->getDisplayName(),
      ]);
      $team->delete();
    }
  }

  /**
   * Handle the deletion of a team.
   */
  public function handleTeamDelete(TeamNode $team) {
    $projects = $this->getProjects($team);
    foreach ($projects as $project) {
      $this->logger->info('Deleting project @project because team @team was deleted.', [
        '@project' => $project->label(),
        '@team' => $team->label(),
      ]);
      $project->delete();
    }
  }

  /**
   * Get a specific VCS provider connection.
   */
  public function getVcsConnection(TeamNode $node, string $provider_id) {
    $connections = $this->getVcsConnections($node);
    if (empty($connections[$provider_id])) {
      return FALSE;
    }
    return $connections[$provider_id];
  }

  /**
   * Set a specific VCS provider connection.
   */
  public function setVcsConnection(TeamNode $node, string $provider_id, $token) {
    $connections = $this->getVcsConnections($node);
    // Let's support passing a string here. Not ideal, but it will probably work
    // at least.
    if (is_string($token)) {
      $token = new AccessToken([
        'access_token' => $token,
      ]);
    }
    $connections[$provider_id] = $token;
    $this->state->set(self::getVcsProviderStateKey($node), $connections);
    $this->state->resetCache();
  }

  /**
   * Get all of the connections stored on a team.
   *
   * @return \League\OAuth2\Client\Token\AccessToken[]
   *   An array of access tokens, at least hopefully.
   */
  public function getVcsConnections(TeamNode $node) : array {
    $this->state->resetCache();
    return $this->state->get(self::getVcsProviderStateKey($node), []);
  }

  /**
   * Remove one connection, by key.
   *
   * Please note that this will complete and not fail even if the connection
   * does not currently exist.
   */
  public function removeVcsConnection(TeamNode $node, string $provider_id) {
    $connections = $this->getVcsConnections($node);
    if (empty($connections[$provider_id])) {
      return;
    }
    unset($connections[$provider_id]);
    $this->setVcsConnections($node, $connections);
  }

  /**
   * Setter for all connections.
   */
  public function setVcsConnections(TeamNode $node, array $connections) {
    $this->state->set(self::getVcsProviderStateKey($node), $connections);
    $this->state->resetCache();
  }

  /**
   * Get the state key we use to store connections for it.
   */
  public static function getVcsProviderStateKey(TeamNode $node) : string {
    return sprintf('violinist_teams:vcs_providers:%s', $node->uuid());
  }

  /**
   * Generates the invite hash for a give team, role and timestamp.
   *
   * This function only generates the hash. Doing the actual verification is
   * up to the caller.
   */
  public function getInviteHash(TeamNode $team, string $membership_type, int $timestamp) : string {
    $key = Settings::getHashSalt() . $team->uuid();
    $data = sprintf('%s:%s:%s:%s', $timestamp, $membership_type, $team->uuid(), Crypt::hmacBase64($team->id(), $key));
    return Crypt::hmacBase64($data, $key);
  }

}
