<?php

namespace Drupal\violinist_teams;

/**
 * Value object containing things about plans.
 */
class Plan implements PlanInterface {

  /**
   * The plan type.
   *
   * @var string
   */
  protected $planType;

  /**
   * The max.
   *
   * @var int
   */
  private $maxRepos;

  /**
   * Constructor.
   */
  public function __construct(string $plan_type) {
    $this->planType = $plan_type;
  }

  /**
   * Setter.
   */
  public function setMaxPrivateRepos(int $max) {
    $this->maxRepos = $max;
  }

  /**
   * {@inheritdoc}
   */
  public function getMaxPrivateRepos() : int {
    switch ($this->planType) {
      case self::ENTERPRISE_PLAN:
        if (!$this->maxRepos) {
          $this->maxRepos = 10;
        }
        return $this->maxRepos;

      case self::PREMIUM_PLAN:
        return self::PREMIUM_PLAN_MAX;

      case self::TRIAL_PLAN:
        return self::TRIAL_PLAN_MAX;

      case self::AGENCY_PLAN:
        if (!$this->maxRepos) {
          $this->maxRepos = 10;
        }
        return $this->maxRepos;

      default:
        return self::REGULAR_PLAN_MAX;
    }
  }

  /**
   * Getter for plan id.
   */
  public function getId() {
    return $this->planType;
  }

}
